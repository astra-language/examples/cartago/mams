package mams.artifacts;

import mams.utils.*;
import mams.web.WebServer;

import cartago.*;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;



@ARTIFACT_INFO(outports = { @OUTPORT(name = "out-1") })
public class PassiveListArtifact extends ListArtifact {
	public void handleGet(ChannelHandlerContext ctx, FullHttpRequest request) {
        try{
            ArrayNode arrayNode = mapper.createArrayNode();
            for (ResourceArtifact childArtifact : handler.getChildren()) {
                arrayNode.add(((ListItemArtifact) childArtifact).createItemNode(new ResourceArtifact[]{}));
            }

            ObjectNode typeNode = mapper.createObjectNode();
            typeNode.set(className, arrayNode);

            ObjectNode selfNode = mapper.createObjectNode();
            selfNode.put("href", getUri());
            ObjectNode links = mapper.createObjectNode();
            links.set("self", selfNode);

            ObjectNode node = mapper.createObjectNode();
            node.set("_embedded", typeNode);
            node.set("_links", links);
            node.put("_total", arrayNode.size());

            String json = mapper.writeValueAsString(node);
            
            WebServer.writeResponse(ctx, request, HttpResponseStatus.OK, "application/hal+json", json);
       }catch( Exception e){
           e.printStackTrace();
           WebServer.writeResponse(ctx, request, HttpResponseStatus.INTERNAL_SERVER_ERROR, "text/plain", e.getMessage());
       }

	}
    
    public void handlePost(ChannelHandlerContext ctx, FullHttpRequest request, String body) {
        Object data = null;
        String name = null;
        
        try {
            data = mapper.readValue(body, Class.forName(className));
            name = Utils.getIdentifier(className,data);
        } catch (Exception e) {
            e.printStackTrace();
            WebServer.writeResponse(ctx, request, HttpResponseStatus.INTERNAL_SERVER_ERROR, "plain/text", e.getMessage());
            return;
        }
        
        try {
            OpFeedbackParam<ArtifactId> id = new  OpFeedbackParam<ArtifactId>();
            CartagoBackend.getInstance().
                doAction(new Op("makeArtifact", getId().getName()+"-"+name, "mams.artifacts.PassiveListItemArtifact", new Object[] {className, data}, id));
            CartagoBackend.getInstance().doAction(new Op("linkArtifacts", id.get(), "out-1", this.getId()));
            CartagoBackend.getInstance().doAction(id.get(), new Op("addRoute", name));
            CartagoBackend.getInstance().doAction(this.getId(), new Op("update", "size", size()+1));
        } catch(Exception e){
            e.printStackTrace();
        }
        
        WebServer.writeResponse(ctx, request, HttpResponseStatus.OK, "application/json", "");
	}
}
