package mams.web;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.handler.codec.http.HttpResponseStatus;

public class ResponseObject {
	String type="text/html";
	String location="";
	HttpResponseStatus status=HttpResponseStatus.OK;
	ByteBuf content=Unpooled.wrappedBuffer(new byte[0]);
	int length=0;
}
