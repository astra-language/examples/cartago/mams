package mams.artifacts;

import mams.utils.CartagoBackend;
import mams.web.WebServer;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import java.lang.reflect.Field;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import cartago.ARTIFACT_INFO;
import cartago.OUTPORT;
import cartago.Op;

@ARTIFACT_INFO(outports = { @OUTPORT(name = "out-1") })
public class PassiveItemArtifact extends AbstractItemArtifact {
	public void handleGet(ChannelHandlerContext ctx, FullHttpRequest request) {
        try {
            ObjectNode node = createItemNode(handler.getChildren());
            String json = mapper.writeValueAsString(node);
            
            WebServer.writeResponse(ctx, request, HttpResponseStatus.OK, "application/hal+json", json);
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
    
	public void handlePut(ChannelHandlerContext ctx, FullHttpRequest request, String body) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            Class<?> myClass = Class.forName(className);
            Object object = mapper.readValue(body, myClass);
            
            Field[] fields = myClass.getFields();
            for (Field field : fields) {
                if (field.get(object) != null) 
                    CartagoBackend.getInstance().doAction(this.getId(), new Op("update", field.getName(),field.get(object)));
            }
            
            WebServer.writeResponse(ctx, request, HttpResponseStatus.OK, "application/json", "");
        } catch (Exception e) {
            e.printStackTrace();
        }
	}

}