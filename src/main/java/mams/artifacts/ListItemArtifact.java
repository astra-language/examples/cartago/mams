package mams.artifacts;

import cartago.*;

public abstract class ListItemArtifact extends AbstractItemArtifact {
    @OPERATION
    void destroyArtifact(){
        try {
			execLinkedOp("out-1", "detach", this.name);
		} catch (OperationException e) {
			e.printStackTrace();
		}
    }
}
