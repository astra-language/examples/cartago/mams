package mams.artifacts;

import cartago.*;

public abstract class ListArtifact extends ResourceArtifact {
    protected String className;
	
	@OPERATION
	void init(String name, String className) {
        this.name = name;
        this.className = className;
        defineObsProperty("size", 0);
    }
    
    protected int size() {
        return getObsProperty("size").intValue();
    }
}
