package mams.artifacts;

import mams.handlers.ResourceHandler;
import mams.web.Handler;
import mams.web.WebServer;
import cartago.*;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;

import com.fasterxml.jackson.databind.ObjectMapper;

public abstract class ResourceArtifact extends Artifact {
    protected ResourceHandler handler;
	protected ObjectMapper mapper = new ObjectMapper();
    private String uri;
    protected String name;

    public String getUri() {
        return uri;
    }
    
    @LINK
	public void attach(String id, Handler childHandler, OpFeedbackParam<String> baseUri) {
        baseUri.set(this.uri);
        handler.addRoute(id, childHandler);
        this.signal("listItemArtifactCreated", id);
	}
	
	@LINK
	public void detach(String id){
		handler.deleteRoute(id);
	}

	@OPERATION
    void update(String name, Object value) {
        this.updateObsProperty(name, value);
	}
	
    @OPERATION
	void createRoute() {
        handler = new ResourceHandler(this);
		try {
            OpFeedbackParam<String> baseUri = new OpFeedbackParam<>();
			execLinkedOp("out-1", "attach", name, handler, baseUri);
            this.uri=baseUri.get() +"/"+ name;
            System.out.println("ResourceArtifact: " + uri);
		} catch (OperationException e) {
			e.printStackTrace();
		}
        
	}
	
  	public void handleGet(ChannelHandlerContext ctx, FullHttpRequest request){
		WebServer.writeResponse(ctx, request, HttpResponseStatus.FORBIDDEN, "application/json", "{}");
	}
    
  	public void handlePut(ChannelHandlerContext ctx, FullHttpRequest request, String body){
		WebServer.writeResponse(ctx, request, HttpResponseStatus.FORBIDDEN, "application/json", "");
	}
	
	public void handlePost(ChannelHandlerContext ctx, FullHttpRequest request, String body){
		WebServer.writeResponse(ctx, request, HttpResponseStatus.FORBIDDEN, "application/json", "");
	}
	
	public void handleDelete(ChannelHandlerContext ctx, FullHttpRequest request, String body){
		WebServer.writeResponse(ctx, request, HttpResponseStatus.FORBIDDEN, "application/json", "");
	}
}
