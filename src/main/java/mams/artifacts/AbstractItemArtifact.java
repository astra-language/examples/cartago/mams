package mams.artifacts;

import java.lang.reflect.Field;

import com.fasterxml.jackson.databind.node.ObjectNode;

import cartago.OPERATION;
import mams.utils.DefaultValue;

public abstract class AbstractItemArtifact extends ResourceArtifact {
    protected String className;
    
	@OPERATION
	void init(String name, String className) {
        this.name = name;
        this.className = className;

        try {
            Class<?> myClass = Class.forName(className);
            Field[] fields = myClass.getFields();
            for (Field field : fields) {
                this.defineObsProperty(field.getName(), DefaultValue.forClass(field.getType()));
            }
        } catch (Exception e) {
            System.out.println("Failed to create DataArtifact for: " + className);
            e.printStackTrace();
        }
	}
    
	@OPERATION
	void init(String name, String className, Object data) {
        this.name = name;
        this.className = className;
        
        try {
            Class<?> myClass = Class.forName(className);
            Field[] fields = myClass.getFields();
            for (Field field : fields) {
                this.defineObsProperty(field.getName(), field.get(data));
            }
        } catch (Exception e) {
            System.out.println("Failed to create ItemArtifact for: " + className);
            e.printStackTrace();
        }
	}

     protected Object getObject() {
        try {
            Class<?> myClass = Class.forName(className);
            
            Object object = myClass.newInstance();
            Field[] fields = myClass.getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(true);
                field.set(object, this.getObsProperty(field.getName()).getValue());
            }
            return object;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }  
    
    protected ObjectNode createItemNode(ResourceArtifact[] children) {
        ObjectNode node = mapper.valueToTree(getObject());
        ObjectNode selfNode = mapper.createObjectNode();
        selfNode.put("href", getUri());
        ObjectNode links = mapper.createObjectNode();
        links.set("self", selfNode);
        for (ResourceArtifact child : children) {
            ObjectNode linkNode = mapper.createObjectNode();
            linkNode.put("href", child.getUri());
            links.set(child.name, linkNode);
        }
        node.set("_links", links);
        return node;
    }
    
    protected boolean isInvalidIdentifier(Field field, Object object, Object value) throws IllegalAccessException {
        return field.get(object) != null && !field.get(object).equals(value);
    }
}